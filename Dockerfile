FROM python:alpine
WORKDIR /flask
COPY . /flask
RUN pip install -r pip-requirements.txt
CMD [ "python", "app.py" ]