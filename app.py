import os
from flask import Flask, jsonify

app = Flask(__name__)
app.secret_key="acf016e15d4b16b7eeeed1c6996cf95ca79f477bb921726fe633718284275299"

information={"Version":"1.0","Date":"2023-09-28","Description":"Despliegue sencillo de Flask de prueba","Author":"Agu"}

@app.route('/')
def index():
	return "Flask test... OK. \U00002705"

@app.route('/about')
def about():
	return jsonify(information)
	
if __name__ == "__main__":
	app.run(host='0.0.0.0',
		 port=os.getenv('APP_PORT',5000))
